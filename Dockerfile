FROM alpine:3.15.0 as selenium-server

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Tiara Rodney (victoryk.it)"
LABEL org.label-schema.name="trodneyio/docker-selenium-server"
LABEL org.label-schema.description="Containerized Selenium Server Environment"
LABEL org.label-schema.vcs-url="https://bitbucket.org/victorykit/docker-selenium-grid"
LABEL org.label-schema.docker.cmd="docker run {image-id} {selenium-server-posargs}"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE

ARG BUILD_VERSION
ARG BUILD_DATE

ARG SELENIUM_JAR_URL=https://github.com/SeleniumHQ/selenium/releases/download/selenium-4.1.0/selenium-server-4.1.2.jar

RUN apk update && \
    apk add openjdk17-jre-headless=17.0.2_p8-r0

WORKDIR /usr/local/share/java
RUN apk add --virtual=build curl && \
    curl -O -J -L $SELENIUM_JAR_URL && ln -s $(basename $SELENIUM_JAR_URL) selenium-server.jar; \
    apk del --purge build

WORKDIR /usr/local/bin
RUN echo '#!/usr/bin/env sh' >> selenium-server.sh && \
    echo 'java -jar /usr/local/share/java/selenium-server.jar $@' >> selenium-server.sh && \
    ln -s selenium-server.sh selenium-server && \
    chmod +x selenium-server.sh selenium-server

WORKDIR /

ENTRYPOINT ["selenium-server"]

################################################################################
# Chromium Webdriver Stage
################################################################################
FROM selenium-server AS chromium
RUN apk add chromium=93.0.4577.82-r2 chromium-chromedriver=93.0.4577.82-r2

################################################################################
# Firefox Webdriver Stage
################################################################################
FROM selenium-server AS firefox
ARG GECKODRIVER_URL=https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
RUN apk add firefox-esr=91.5.0-r0 xvfb
WORKDIR /tmp
RUN apk add --virtual=build curl && \
    curl -O -J -L $GECKODRIVER_URL && \
    tar -xzvf $(basename $GECKODRIVER_URL) && \
    cp ./geckodriver /usr/local/bin/ && \
    rm geckodriver $(basename $GECKODRIVER_URL) && \
    chmod a+x /usr/local/bin/geckodriver; \
    apk del --purge build
WORKDIR /